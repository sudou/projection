# a projection approach for TopOpt

This repository collects the codes for the paper "A projection approach for topology optimization of porous structures through implicit local volume control"