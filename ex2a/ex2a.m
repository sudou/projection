clc,clear
!rm *.mat *.txt *.fig *.png
stru=5;
gv=1;
eta1=0.5;
eta2=0.6;
eta3=[];
weight=[];
saveflag = 1;
betascale = 2;
nely=100;
nelx=50;
r1=3;
r2=6;
loopmax = 100;
xval0=0.5;
%
erosion=0;
[xval,xPhys,xSum,xBox,c,v,b]=....
    top71mma4(nelx,nely,gv,0,3,r1,r2,eta1,eta2,8,....
    128,stru,betascale,1,xval0,50,1e-4,1e-5,eta3,loopmax,saveflag,eta1+0.01*erosion); %tol should be small
!mkdir leaf20190427
!mv *.mat *.txt *.fig *.png leaf20190427
!cp *.m leaf20190427
%%
nely=400;
nelx=200;
r1=12;
r2=24;
loopmax=400;
xval0=kron(xval,ones(4));%0.6;% can be too small or too large
erosion=0;
[xval,xPhys,xSum,xBox,c,v,b]=....
    top71mma4(nelx,nely,gv,0,3,r1,r2,eta1,eta2,8,....
    128,stru,betascale,1,xval0,50,1e-4,1e-5,eta3,loopmax,saveflag,eta1+0.01*erosion); %tol should be small
!mv *.mat *.txt *.fig *.png leaf20190427
erosion=3;
[xval,xPhys,xSum,xBox,c,v,b]=....
    top71mma4(nelx,nely,gv,0,3,r1,r2,eta1,eta2,8,....
    128,stru,betascale,1,xval0,50,1e-4,1e-5,eta3,loopmax,saveflag,eta1+0.01*erosion); %tol should be small
!mv *.mat *.txt *.fig *.png leaf20190427