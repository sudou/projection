%%%% A 71 LINE TOPOLOGY OPTIMIZATION CODE BASED ON CONV2 FILTERING Nov, 2010 %%%%
function [xval,xPhys,xAve,xSum,xBox,c,v,b]=top71mma4(nelx,nely,volfrac,weight,penal,rmin,rmin2,eta,boxeta,beta221,betamax,stru,beta11,beta0,xval0,loopmod,changetol,tol,boxeta2,loopmax,keepfig,eta2)
name = sprintf('%02d',fix(clock));
fid=fopen([name '.txt'],'wt');tic
fprintf(fid,'weight %10.3g\n',weight);
sharpness=@(x) mean(x(:)-x(:).^2)*4;
%% MATERIAL PROPERTIES
E0 = 1;
Emin = 1e-9;
nu = 0.3;
%% MMA PROPERTIES
mmma = 1;
nmma = nelx * nely;
xmin(1:nmma,1) = 0;
xmax(1:nmma,1) = 1;
[low,upp,xold1,xold2] = deal([]);
[df0dx2(1:nmma,1),dfdx2(1:mmma,1:nmma)]= deal(0);
%% PREPARE SUBFUNCTIONS
[dens,sens] = filtersub3(nely,nelx,rmin,rmin2);
%% PREPARE FINITE ELEMENT ANALYSIS
A11 = [12  3 -6 -3;  3 12  3  0; -6  3 12 -3; -3  0 -3 12];
A12 = [-6 -3  0  3; -3 -6 -3 -6;  0 -3 -6  3;  3 -6  3 -6];
B11 = [-4  3 -2  9;  3 -4 -9  4; -2 -9 -4 -3;  9  4 -3 -4];
B12 = [ 2 -3  4 -9; -3  2  9 -2;  4  9  2  3; -9 -2  3  2];
KE = 1/(1-nu^2)/24*([A11 A12;A12' A11]+nu*[B11 B12;B12' B11]);
nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
edofVec = reshape(2*nodenrs(1:end-1,1:end-1)+1,nelx*nely,1);
edofMat = repmat(edofVec,1,8)+repmat([0 1 2*nely+[2 3 0 1] -2 -1],nelx*nely,1);
iK = reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1);
jK = reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1);
U=zeros(2*(nely+1)*(nelx+1),1); F=[];freedofs=[]; get_loads_suports;

%% INITIALIZE ITERATION
get_xval
loop   = 0;
change = 1;
beta   = beta0;
sharp  = 1;
loopbeta = 0;
%% START ITERATION
while change>tol && loop<loopmax
    loop=loop+1; loopbeta=loopbeta+1; 
    boxbeta=beta*beta221; %%%%%%
    boxbeta=min(beta*beta221,betamax); %%%%%%    
    %%robust design
    [dc,dv,c,v,xTilde,xMin,xSum,xBox,xPhys,xAve]=robust(eta2);
    %% MMA UPDATE OF DESIGN VARIABLES AND PHYSICAL DENSITIES
    if loop<2, c0=c; end;
    f0val  = c/c0;
    df0dx  = dc/c0;
    fval   = v-1;
    dfdx   = dv;
    xmin = max(0,xval(:)-0.1);xmax = min(1,xval(:)+0.1);
    [xmma,~,~,~,~,~,~,~,~,low,upp,~,~] = ...
        mmasub(mmma,nmma,loop,xval(:),xmin,xmax,xold1,xold2, ...
        f0val,df0dx,df0dx2,fval,dfdx,dfdx2,low,upp,1,0,1000,0);
    xold2 = xold1; xold1 = xval(:);
    change = max(abs(xmma(:)-xval(:)))
    xval(:) = xmma;
    increase_beta
    %if loopbeta>49 && change<tol, break, end
    %if loopbeta>49 && beta>=betamax, break, end
    
    if nargin>20
        colormap gray; imagesc(1-xPhys); caxis([0 1]); axis equal; axis off; drawnow;
        if keepfig && mod(loop,50)==0
            %colormap gray; imagesc(1-xPhys); caxis([0 1]); axis equal; axis off; drawnow;
            savefig(gcf,[name '_xPhys_' num2str(loop) '.fig'],'compact');
            print(gcf,'-r900',[name '_xPhys' num2str(loop)],'-dpng');
            colormap gray; imagesc(1-xBox); caxis([0 1]); axis equal; axis off; drawnow;
            savefig(gcf,[name '_xBox_' num2str(loop) '.fig'],'compact');
            print(gcf,'-r900',[name '_xBox_' num2str(loop)],'-dpng');
            colormap gray; imagesc(1-xMin); caxis([0 1]); axis equal; axis off; drawnow;
            savefig(gcf,[name '_xMin_' num2str(loop) '.fig'],'compact');
            print(gcf,'-r900',[name '_xMin_' num2str(loop)],'-dpng');
        end
    end
    %% PRINT RESULTS
    sharp=sharpness(xPhys); b=mean(xBox(:));
    fprintf(fid,' It.:%3d Obj.:%8.4f Vol.:%6.4f %6.4f ch.:%9.3e beta.:%4.1f boxbeta.:%4.1f penal.:%3.1f sharpness:%7.4f\n',loop,c,v,b,change,beta,boxbeta,penal,sharp);
    fprintf(' It.:%3d Obj.:%8.4f Vol.:%6.4f %6.4f ch.:%9.3e beta.:%4.1f boxbeta.:%4.1f penal.:%3.1f sharpness:%7.4f\n',loop,c,v,b,change,beta,boxbeta,penal,sharp);
    %% PLOT DENSITIES
    %{
    subplot(221)
    colormap gray; imagesc(1-xPhys); caxis([0 1]); axis equal; axis off; drawnow;
    subplot(222)
    histogram(xPhys)
    subplot(223)
    histogram(xMin)
    subplot(224)
    histogram(xBox)
    %}
end
ci = compliance(xPhys);%intermediate
    fprintf(fid,' It.:%3d Obj.:%8.4f Vol.:%6.4f %6.4f ch.:%9.3e beta.:%4.1f boxbeta.:%4.1f penal.:%3.1f sharpness:%7.4f\n',loop,ci,v,b,change,beta,boxbeta,penal,sharp);
    fprintf(' It.:%3d Obj.:%8.4f Vol.:%6.4f %6.4f ch.:%9.3e beta.:%4.1f boxbeta.:%4.1f penal.:%3.1f sharpness:%7.4f\n',loop,ci,v,b,change,beta,boxbeta,penal,sharp);
    %% PLOT DENSITIES
fprintf(fid,'Elapsed time is %5.f seconds, about %5.f minutes',toc,toc/60);
fclose(fid);
save(name)
    function c=compliance(xPhys)
        %% FE-ANALYSIS
        sK = reshape(KE(:)*(Emin+xPhys(:)'.^penal*(E0-Emin)),64*nelx*nely,1);
        K = sparse(iK,jK,sK); K = (K+K')/2;
        U(freedofs) = K(freedofs,freedofs)\F(freedofs);
        %% OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        ce = reshape(sum((U(edofMat)*KE).*U(edofMat),2),nely,nelx);
        c  = sum(sum((Emin+xPhys.^penal*(E0-Emin)).*ce));
    end
%% eta2 -> dc,dv
    function [dc,dv,c,v,xTilde,xMin,xSum,xBox,xPhys,xAve]=robust(eta2)
        %% FILTERING/MODIFICATION OF DENSITIES
        [xTilde,xMin,xMinE,xSum,xBox,xPhys,xPhysE,xAve,XAveE]=dens(xval,beta,eta,eta2,boxbeta,boxeta,boxeta2);
        %% FE-ANALYSIS
        sK = reshape(KE(:)*(Emin+xPhysE(:)'.^penal*(E0-Emin)),64*nelx*nely,1);
        K = sparse(iK,jK,sK); K = (K+K')/2;
        U(freedofs) = K(freedofs,freedofs)\F(freedofs);
        %% OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        ce = reshape(sum((U(edofMat)*KE).*U(edofMat),2),nely,nelx);
        c  = sum(sum((Emin+xPhysE.^penal*(E0-Emin)).*ce));
        v  = mean(xPhys(:))/volfrac;
        %%
        d0 = -penal*(E0-Emin)*xPhysE.^(penal-1).*ce;
        d1 = d0.*xBox;
        d2 = d0.*xMinE;
        d3 = ones(nely,nelx)/(volfrac*nelx*nely);
        d4 = d3.*xBox;
        d5 = d3.*xMin;
        %% FILTERING/MODIFICATION OF SENSITIVITIES
        [dc,dv]=sens(d1,d2,d4,d5,xTilde,xSum,beta,eta,eta2,boxbeta,boxeta,boxeta2);
    end
    
%% initilize xval
    function get_xval
        if ~isscalar(xval0)
            xval = xval0;
        elseif xval0<=1
            xval = repmat(xval0,nely,nelx);
        elseif xval0>1
            rng('default')
            xval= rand(nely,nelx);
        end
    end

%% change or terimnate
    function increase_beta
        if beta<betamax && (loopbeta>=loopmod || change<changetol)
            loopbeta=0;
            beta = min(beta*beta11,betamax);
            penal = min(penal+0.25,3);
        end
    end

%% library of structures
    function get_loads_suports
        switch stru
            case 1
                mbb;
            case 2
                cantilever;
            case 3
                bridge;
            case 4
                shell;
            case 5
                mbb_bottom_load;
            case 6 
                cantilever_half;
        end
        
        function mbb
            % DEFINE LOADS AND SUPPORTS (HALF MBB-BEAM)
            F = sparse(2,1,-1,2*(nely+1)*(nelx+1),1);
            fixeddofs = union([1:2:2*(nely+1)],[2*(nelx+1)*(nely+1)]);
            alldofs = [1:2*(nely+1)*(nelx+1)];
            freedofs = setdiff(alldofs,fixeddofs);
        end
        
        function mbb_bottom_load
            % DEFINE LOADS AND SUPPORTS (HALF MBB-BEAM)
            F = sparse(2*(nely+1),1,-1,2*(nely+1)*(nelx+1),1);
            fixeddofs = union([1:2:2*(nely+1)],[2*(nelx+1)*(nely+1)]);
            alldofs = [1:2*(nely+1)*(nelx+1)];
            freedofs = setdiff(alldofs,fixeddofs);
        end
        
        function cantilever
            % DEFINE LOADS AND SUPPORTS (CANTILEVER)
            F = sparse(nelx*(2*nely+2)+nely+2,1,-1,2*(nely+1)*(nelx+1),1);
            fixeddofs = 1:2*nely+2;
            alldofs = 1:2*(nely+1)*(nelx+1);
            freedofs = setdiff(alldofs,fixeddofs);
        end
        
        function cantilever_half
            % DEFINE LOADS AND SUPPORTS (CANTILEVER_HALF)
            F = sparse((nelx+1)*(2*nely+2),1,-1,2*(nely+1)*(nelx+1),1)
            fixeddofs = [1:2*nely+2 (2*nely+2)*(1:nelx+1)-1];
            alldofs = [1:2*(nely+1)*(nelx+1)];
            freedofs = setdiff(alldofs,fixeddofs);
        end
        
        function bridge
            % DEFINE LOADS AND SUPPORTS (BRIDGE)
            F = sparse(2*(nely+1),1,-1,2*(nely+1)*(nelx+1),1);
            fixeddofs = union([1:2:2*(nely+1)],[2*(nelx+1)*(nely+1)]);
            alldofs = [1:2*(nely+1)*(nelx+1)];
            freedofs = setdiff(alldofs,fixeddofs);
        end
        
        function shell
            % DEFINE LOADS AND SUPPORTS (SHELL)
            F = zeros(2*(nely+1)*(nelx+1),1);
            F((0:nelx)*(2*nely+2)+2)=-1;
            F((1:nelx+1)*(2*nely+2))=1;
            fixeddofs = [nely+(1:2) nelx*(2*nely+2)+nely+2];
            alldofs = [1:2*(nely+1)*(nelx+1)];
            freedofs = setdiff(alldofs,fixeddofs);
        end
    end

end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This Matlab code was written by Suguang Dou                              %         %
% Please sent your comments to: dousuguang@gmail.com                       %
%                                                                          %                                                       %
% Disclaimer:                                                              %
% The authors reserves all rights but do not guaranty that the code is     %
% free from errors. Furthermore, we shall not be liable in any event       %
% caused by the use of the program.                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%{
% DEFINE LOADS AND SUPPORTS (HALF MBB-BEAM)
F = sparse(2,1,-1,2*(nely+1)*(nelx+1),1);
U = zeros(2*(nely+1)*(nelx+1),1);
fixeddofs = union([1:2:2*(nely+1)],[2*(nelx+1)*(nely+1)]);
alldofs = [1:2*(nely+1)*(nelx+1)];
freedofs = setdiff(alldofs,fixeddofs);
%}

%{
% DEFINE LOADS AND SUPPORTS (CANTILEVER-BEAM)
F = sparse(2*(nely+1)*nelx+nely+2,1,-1,2*(nely+1)*(nelx+1),1);
U = zeros(2*(nely+1)*(nelx+1),1);
fixeddofs = 1:2*(nely+1);
alldofs   = 1:2*(nely+1)*(nelx+1);
freedofs  = setdiff(alldofs,fixeddofs);
%}
