%version3 written by Suguang Dou on June 2018 Reading
%(1) combine the operations in forward/backward chains
%(2) use function handles
function [f1,f2] = filtersub3(nely,nelx,rmin,rmin2)
%example:
%[dens,sens] = filtersub(nely,nelx,rmin,eta);
%[xTilde,xPhys]=dens(xval,beta);
%[df0dx,dfdx]=sens(df0dx,dfdx,xTilde,beta);

%2D-Filter-Generation
d = -ceil(rmin)+1:ceil(rmin)-1;
[dy,dx] = meshgrid(d);
h = max(0,rmin-sqrt(dx.^2+dy.^2));
H = conv2(ones(nely,nelx),h,'same');

%2D-Filter-Generation
d = -ceil(rmin2):ceil(rmin2);
[dy,dx] = meshgrid(d);
box = double(sqrt(dx.^2+dy.^2)<=rmin2);
%boxN = sum(sum(box));
boxN = conv2(ones(nely,nelx),box,'same');

P = @(x,eta,beta) (tanh(beta*eta)+tanh(beta*(x-eta)))./(tanh(beta*eta)+tanh(beta*(1-eta)));
dP = @(x,eta,beta) beta*(1-tanh(beta*(x-eta)).^2)./(tanh(beta*eta)+tanh(beta*(1-eta)));

%return function handles
[f1,f2]=deal(@dens,@sens);

    function [X1,X2,X2e,X3,X4,X5,X5e,X6,X6e]=dens(X,beta,eta,eta2,boxbeta,boxeta,boxeta2)
        %filtering of density
        X1 = conv2(X,h,'same')./H;
        %projection of density
        X2 = P(X1,eta,beta);
        %projection of density
        X2e = P(X1,eta2,beta);
        %sum of X2
        X3 = conv2(X2,box,'same')./boxN;
        %projection of sum
        X4 = (1-P(X3,boxeta,boxbeta));
        %product
        X5 = X2.*X4;
        X5e = X2e.*X4;
        %sum of X5
        X6 = conv2(X5,box,'same')./boxN;
        X6e = conv2(X5e,box,'same')./boxN;

    end

    function [d1,d4]=sens(d1,d2,d4,d5,xTilde,xSum,beta,eta,eta2,boxbeta,boxeta,boxeta2)
        %diff_xPhys_xTilde
        dx = dP(xTilde,eta,beta);
        de = dP(xTilde,eta2,beta);
        if isempty(boxeta2)
            dxx=-dP(xSum,boxeta,boxbeta);
        else
            x1 = P(xSum,boxeta2,boxbeta);
            x2 = 1-P(xSum,boxeta,boxbeta);
            dx1 = dP(xSum,boxeta2,boxbeta);
            dx2 = -dP(xSum,boxeta,boxbeta);
            dxx = x1.*dx2+x2.*dx1;
        end
        %dc
        d1=d1+conv2(d2.*dxx./boxN,box,'same');
        d1=conv2(d1.*de./H,h,'same');
        d1=reshape(d1,[],1);
        %dv
        d4=d4+conv2(d5.*dxx./boxN,box,'same');
        d4=conv2(d4.*dx./H,h,'same');
        d4=reshape(d4,1,[]);
    end

end
