function [f1,f2] = filtersub(nelx,nely,rmin,rmax)
%output:
%f1 and f2 are two function handles for densities and sensitivities.
%example:
%[dens,sens] = filtersub(nelx,nely,rmin,rmax);
%[X1,X2,X2e,X3,X4,X5,X5e,X6,X6e]=dens(X,beta,eta,etaPlus,beta2,eta2);
%[dc,dv]=sens(dc,dv,X1,X2,X2e,X3,X4,beta,eta,etaPlus,beta2,eta2);

%2D-Filter-Generation
d = -ceil(rmin)+1:ceil(rmin)-1;
[dy,dx] = meshgrid(d);
h = max(0,rmin-sqrt(dx.^2+dy.^2));
Hs = conv2(ones(nely,nelx),h,'same');

%2D-Filter-Generation
d = -ceil(rmax):ceil(rmax);
[dy,dx] = meshgrid(d);
n = double(sqrt(dx.^2+dy.^2)<=rmax);
Ns = conv2(ones(nely,nelx),n,'same');

%function handle for projection and its derivative
P = @(x,eta,beta) (tanh(beta*eta)+tanh(beta*(x-eta)))./(tanh(beta*eta)+tanh(beta*(1-eta)));
dP = @(x,eta,beta) beta*(1-tanh(beta*(x-eta)).^2)./(tanh(beta*eta)+tanh(beta*(1-eta)));

%return function handles
[f1,f2]=deal(@dens,@sens);

%nested function for densities
function [X1,X2,X2e,X3,X4,X5,X5e,X6,X6e]=dens(X,beta,eta,etaPlus,beta2,eta2)
%density filter
X1 = conv2(X,h,'same')./Hs;
%projection of X1
X2 = P(X1,eta,beta);
%erosion projection of X1
X2e = P(X1,etaPlus,beta);
%local volume fraction of X2
X3 = conv2(X2,n,'same')./Ns;
%projection of X3
X4 = 1-P(X3,eta2,beta2);
%product for standard design
X5 = X2.*X4;
%product for erosion design
X5e = X2e.*X4;
%local volume fraction of X5
X6 = conv2(X5,n,'same')./Ns;
%local volume fraction of X5e
X6e = conv2(X5e,n,'same')./Ns;
end %dens

%nested function for sensitivities
function [dc,dv]=sens(dc,dv,X1,X2,X2e,X3,X4,beta,eta,etaPlus,beta2,eta2)
%diff projection 1
dX2  =  dP(X1,eta,beta);
%diff erosion projection 1
dX2e =  dP(X1,etaPlus,beta);
%diff projection 2
dX4  = -dP(X3,eta2,beta2);
%dc
d2 = dc.*X4; d4 = dc.*X2e;
dc = d2 + conv2(d4.*dX4./Ns,n,'same');
dc = conv2(dc.*dX2e./Hs,h,'same');
dc = reshape(dc,[],1);
%dv
d2 = dv.*X4; d4 = dv.*X2;
dv = d2 + conv2(d4.*dX4./Ns,n,'same');
dv = conv2(dv.*dX2./Hs,h,'same');
dv = reshape(dv,1,[]);
end %sens

end %filtersub